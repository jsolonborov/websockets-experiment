import { Server } from "socket.io";
import Fastify from "fastify";
import { TypeBoxTypeProvider } from "@fastify/type-provider-typebox";
import {
  Command,
  CommandType,
  OfferReply,
  OfferReplyType,
  OfferRequest,
  OfferRequestType,
} from "./schema/Command";
import cors from "@fastify/cors";

const app = Fastify({
  logger: true,
}).withTypeProvider<TypeBoxTypeProvider>();

const io = new Server(app.server, {
  cors: {
    origin: "*",
  },
});

const clients: Map<string, string> = new Map();

io.on("connection", (socket) => {
  console.log("New connection %s", socket.id);
  socket.emit("INIT", "Session initialized, the ID is: " + socket.id);

  socket.on("register", (clientId: string) => {
    clients.set(clientId, socket.id);
    console.log("Registered client %s with socket id %s", clientId, socket.id);
  });
});

io.on("disconnect", (socket) => {
  console.log("Disconnected %s", socket.id);
  clients.forEach((value, key) => {
    if (value === socket.id) {
      clients.delete(key);
      console.log("Client %s disconnected", key);
    }
  });
});

app.register(cors, {
  origin: "*",
});

app.post<{ Body: CommandType }>(
  "/ping",
  { schema: { body: Command } },
  async (request, reply) => {
    const { clientId } = request.body;
    const socketId = clients.get(clientId);
    if (socketId) {
      io.to(socketId).emit("PING", "PING");
      reply.send("PONG");
    } else {
      reply.status(404).send("Client not found");
    }
  }
);

app.post<{ Body: OfferRequestType; Response: { 200: OfferReplyType } }>(
  "/request-offer",
  {
    schema: {
      body: OfferRequest,
      response: {
        200: OfferReply,
      },
    },
  },
  async (request, reply) => {
    const clientId = request.body.clientId;
    const socketId = clients.get(clientId);
    console.log("Requesting offer for client %s", clientId);

    if (!socketId) {
      return reply.status(404).send("Client not found");
    }

    const socket = io.sockets.sockets.get(socketId);
    if (!socket) {
      return reply.status(404).send("Client not found");
    }

    io.to(socketId).emit("request-offer", request.body.offer);

    // Wait for the response from the socket
    try {
      const data = await new Promise<OfferReplyType>((resolve, reject) => {
        socket.once("answer", (data: OfferReplyType) => {
          resolve(data);
        });

        // Optional timeout in case the response takes too long
        setTimeout(() => {
          reject(new Error("Timeout waiting for response"));
        }, 10000); // 10 seconds timeout
      });

      console.log(data);
      reply.send({
        answer: data,
      });
    } catch (error) {
      reply.status(500).send({ error });
    }
  }
);

await app.listen({
  port: 9090,
  host: "0.0.0.0",
});
