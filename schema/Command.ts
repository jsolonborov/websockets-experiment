import { Static, Type } from "@sinclair/typebox";

export const Command = Type.Object({
  clientId: Type.String(),
});

export type CommandType = Static<typeof Command>;

export const OfferRequest = Type.Object({
  clientId: Type.String(),
  offer: Type.Object({
    type: Type.String(),
    sdp: Type.String(),
  }),
});

export type OfferRequestType = Static<typeof OfferRequest>;

export const OfferReply = Type.Object({
  answer: Type.Object({
    type: Type.String(),
    sdp: Type.String(),
  }),
});

export type OfferReplyType = Static<typeof OfferReply>;
